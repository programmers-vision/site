import { config, locales } from './config/i18n';
export default {
  target: 'static',
  ssr: true,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Programmers Palace',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'theme-color', content: '#1662bf' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Programmers Palace is a friendly and supportive community for programmers of all levels of experience. We offer help in a variety of programming languages and levels.',
      },
      { property: 'og-site_name', content: 'Programmers Palace' },
      { hid: 'og-type', property: 'og:type', content: 'website' },
      {
        hid: 'og-title',
        property: 'og:title',
        content: 'Programmers Palace | A Programming Community',
      },
      {
        hid: 'og-description',
        name: 'og:description',
        content:
          'Programmers Palace is a friendly and supportive community for programmers of all levels of experience. We offer help in a variety of programming languages and levels.',
      },
      {
        hid: 'og-url',
        property: 'og:url',
        content: 'https://programmerspalace.com',
      },
      {
        hid: 'og-image',
        property: 'og:image',
        content: '/banner.png',
      },
      {
        name: 'twitter:site',
        content: '@PrgmrsPalace',
      },
      {
        name: 'twitter:title',
        content: 'Programmers Palace',
      },
      {
        name: 'twitter:description',
        content:
          'Programmers Palace is a friendly and supportive community for programmers of all levels of experience. We offer help in a variety of programming languages and levels.',
      },
      {
        name: 'twitter:card',
        content: 'summary',
      },
      {
        name: 'twitter:image',
        content: '/banner.png',
      },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Exo:400,700',
      },
    ],
  },
  loading: {
    color: '#1e1e1e',
    height: '3px',
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/styles/main.scss', 'aos/dist/aos.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    [
      '@nuxt/image',
      {
        provider: 'static',
      },
    ],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/buefy
    'nuxt-buefy',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    [
      '@nuxtjs/fontawesome',
      {
        icons: {
          solid: true,
          brands: true,
        },
      },
    ],
    [
      'nuxt-i18n',
      {
        strategy: 'prefix_except_default',
        defaultLocale: 'en',
        locales: Object.keys(locales).map((lang) => locales[lang].definition),
        vueI18n: config,
      },
    ],
  ],

  //https://i18n.nuxtjs.org/
  i18n: {
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      redirectOn: 'root',
    },
    fontAwesome: {
      packs: [
        {
          package: '@fortawesome/free-brands-svg-icons',
        },
      ],
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
      // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
      baseURL: '/',
    },
    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {},
  },
};
