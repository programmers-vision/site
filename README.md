# Programmers Palace Website Frontend

## About

The frontend for the Programmers Palace website can be found in this repository. The project is written in JavaScript and built on Nuxt.js using the Vue user interface library.

## Installation Steps

```bash
# Make sure all dependencies are installed
$ npm install

# Run the website at localhost:3000 with a hot reload
$ npm run dev
```

## Features Overview

1. **Navbar**

    The navbar's purpose is to contain the server's branding while also providing important links that users may be looking for as well as key information such as the current selected language and the number of online members. The navbar is fully responsive and has a mobile version for smaller devices. The navbar blends in with the blue background on the landing section, and a background appears when the user scrolls down.
    * When the language selector is clicked, a dropdown menu appears, allowing users to select the language of their choosing.
    * The interactive online count section displays the number of online members on the server, giving the viewer the impression that there is an active community waiting for them. The online count section features a pulsing green online circle, and has an incrementing animation when first loaded to draw the user's attention. The count can also be clicked to take the user to the Discord server.

2. **Landing Section**

    The landing section includes a large text of the community's name, a description, and a join the server button. The background of the landing area features moving square shapes that flow downwards and change in fullness and transparency as they move. The last component of the landing area is a downward arrow at the bottom of the screen that elegantly slides up and down to encourage the user to proceed to the following section, which contains information about the community.

3. **Information Sections**

    The information section is divided into several segments, each of which addresses a different key question about our community, such as who we are, why join us, and how to join us. Each segment has a heading that displays the question, a subheading that provides a brief answer to the question, and a paragraph that elaborates on the answer. Each segment also includes a graphic that illustrates and provides an example of the answer. The last segment has a call to action button that allows users to join the server after reading about it.

4. **Footer**

    The footer's purpose is to indicate the end of the page while also offering important elements such as the community's socials and a copyright notice.

5. **Other**
* An elegant on scroll animation that is triggered the first time the user scrolls through the page to add an appealing aspect to the page. 
* Fontawesome's icons are also used across the website to draw attention, increase readability, and improve aesthetic appeal. 
* Under the landing area, a wavy SVG shape is utilized to provide a transition from the blue background to the page's content.

-Krista