let navData = [
  {
    name: 'navigation[0]',
    link: '/',
    newTab: false,
    external: false,
  },
  {
    name: 'navigation[1]',
    link: '/staff',
    newTab: false,
    external: false,
  },
  {
    name: 'navigation[2]',
    link: 'https://paypal.me/DevCanter',
    newTab: true,
    external: true,
  },
  {
    name: 'navigation[3]',
    link: '/contact',
    newTab: false,
    external: false,
  },
];

export default navData;
