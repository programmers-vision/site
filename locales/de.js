const discordButton = 'Dem Discord Beitreten';
const staffApplicationTitle = 'Personal Bewerbung';
const contactUsTitle = 'Kontakt';

const language = {
  languageName: 'Deutsch',
  landing: {
    subHeading: 'Eine Programmierer Community',
    buttonText: 'Trete der Community bei',
  },
  navigation: [
    'Startseite',
    staffApplicationTitle,
    'Spenden Sie',
    contactUsTitle,
  ],
  home: {
    section1: {
      heading: 'Wer Sind Wir?',
      subHeading:
        'Eine Gemeinschaft von Entwicklern, die sich gegenseitig helfen',
      paragraph:
        'Programmers Palace ist eine freundliche und unterstützende Gemeinschaft für Programmierer aller Erfahrungsstufen. Wir bieten Hilfe in einer Vielzahl von Programmiersprachen und -stufen. Zusätzlich zum ständigen Live-Support veranstalten wir Events und bieten regelmäßig Programmier- und Webdesign-Kurse an. Wenn Sie sich für das Programmieren begeistern und Gleichgesinnte treffen möchten, ist Programmers Palace der richtige Ort für Sie.',
      buttonText: discordButton,
    },
    section2: {
      heading: 'Warum Bei Uns Mitmachen?',
      subHeading: 'Lernen, Lehren oder einfach Entspannen',
      paragraph:
        'Egal, ob Sie gerade erst anfangen oder schon jahrelange Erfahrung haben, wir haben einen Platz für Sie. Unser Ziel ist es, Spaß am Programmieren zu vermitteln und gleichzeitig ein gastfreundliches soziales Umfeld zu schaffen, in dem Menschen zusammenkommen und ihre Fähigkeiten gemeinsam erweitern können. Schließen Sie sich uns an, um neue Leute kennenzulernen, Ihre Fähigkeiten weiterzuentwickeln, Hilfe zu erhalten oder andere bei der Lösung von Code-Problemen zu unterstützen und an Vorträgen unserer erfahrenen Mitglieder teilzunehmen. Unsere Gemeinschaft wächst schnell, mit Tausenden von Entwicklern aus der ganzen Welt, die zusammenkommen, um ihr Wissen zu erweitern und eine tolle Zeit zu erleben. ',
      buttonText: '',
    },
    section3: {
      heading: 'Ich Bin Interessiert. Wie Kann Ich Beitreten?',
      subHeading: 'Wir sind auf Discord zu finden',
      paragraph:
        'Discord ist unsere bevorzugte Plattform aufgrund seiner Funktionen, Benutzerfreundlichkeit und Zuverlässigkeit. Wenn Sie bereits ein Discord-Konto haben und beitreten möchten, klicken Sie auf die Schaltfläche unten, um den Einladungslink zu erhalten. Wenn Sie noch kein Konto haben, können Sie ganz einfach ein kostenloses Konto erstellen und es bequem auf jeder Plattform, einschließlich Webbrowsern, nutzen. ',
      buttonText: discordButton,
    },
  },
  form: {
    formIncomplete:
      'Bitte füllen Sie die mit einem Sternchen (*) gekennzeichneten Pflichtfelder aus.',
    submissionError:
      'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal oder kontaktieren Sie unser Team.',
    failure:
      'Irgendetwas stimmt nicht, bitte überprüfen Sie die Fehler und passen Sie Ihre Bewerbung an.',
  },
  staffApplication: {
    title: staffApplicationTitle,
    section: {
      heading: '',
      subHeading: 'Möchten Sie Teil unseres Teams werden?',
      paragraph:
        'Wir sind ständig auf der Suche nach aktiven Mitgliedern, die unserem Team beitreten und uns beim Aufbau unserer Gemeinschaft helfen. Unser Team besteht aus mehreren Rollen mit unterschiedlichen Verantwortlichkeiten. Die Teammitglieder arbeiten zusammen, um den Server zu moderieren, den Servermitgliedern zu helfen und zum Wachstum des Servers beizutragen. Wenn Sie unserem Team beitreten möchten, füllen Sie bitte das untenstehende Formular aus und wir werden uns mit Ihnen in Verbindung setzen.',
      buttonText: '',
    },
    discordTag: 'Discord Tag',
    requiredDiscordTag:
      'Ein Discord-Tag ist erforderlich, bitte geben Sie einen gültigen Discord-Tag an.',
    invalidDiscordTag: 'Ungültiger Discord-Tag',
    invalidDiscordTagFull:
      'Ungültiger Discord-Tag. Bitte geben Sie einen gültigen Discord-Tag an.',
    email: 'Email',
    requiredEmail:
      'Eine E-Mail ist erforderlich, bitte geben Sie eine gültige E-Mail Adresse an.',
    invalidEmail: 'Ungültige E-Mail',
    invalidEmailFull:
      'Ungültige E-Mail. Bitte geben Sie eine gültige E-Mail ein.',
    positionType: 'Für welche Stelle möchten Sie sich bewerben?',
    positionRequired: 'Wählen Sie die Stelle, für die Sie sich bewerben.',
    invalidPosition: 'Ungültige Position',
    selectPosition: 'Wählen Sie eine Stelle',
    position: {
      moderator: 'Moderator',
      community: 'Community Engagement',
      teacher: 'Lehrer',
      developer: 'Entwickler',
    },
    timeOnDiscord: 'Wie viel Zeit verbringen Sie pro Tag auf Discord?',
    requiredTimeOnDiscord:
      'Geben Sie die Anzahl der Stunden an, die Sie täglich in Discord verbringen.',
    invalidTimeOnDiscord:
      'Ungültige Stunden. Bitte geben Sie eine gültige Anzahl von Stunden ein.',
    joinReason: 'Warum wollen Sie in unserem Team mitarbeiten?',
    experience:
      'Welche Erfahrungen haben Sie mit der Programmierung? Bei Stellen für soziales Engagement beschreiben Sie bitte auch entsprechende Erfahrungen.',
    whatsSpecial: 'Was unterscheidet Sie von anderen Bewerbern?',
    serverGoals: 'Was sind Ihre persönlichen Ziele oder Ziele für den Server?',
    additional: 'Möchten Sie noch etwas wissenswertes erwähnen?',
    submissionNote:
      '* Bitte kontaktieren Sie Canter, wenn Sie innerhalb einer Woche keine Antwort erhalten.',
    thankYou:
      'Vielen Dank für Ihr Interesse an einer Mitarbeit in unserem Team. Wir werden Sie so bald wie möglich kontaktieren!',
    submit: 'Bewerbung Einreichen',
    success: 'Erfolg',
  },
  contactUs: {
    title: contactUsTitle,
    section: {
      heading: '',
      subHeading: 'Ihre Meinung ist uns wichtig!',
      paragraph:
        'Wir streben danach, die beste Programmier-Community zu sein, und sind immer offen für Feedback und Partnerschaften! Wenn Sie sich mit unserem Team in Verbindung setzen oder uns einfach Ihre Vorschläge, Beschwerden und Kommentare mitteilen möchten, verwenden Sie bitte das unten stehende Formular. Wir freuen uns auf Ihr Feedback zu Ihren Erfahrungen und Ihrer Beziehung zu uns. Wir danken Ihnen im Voraus für Ihre Antwort.',
      buttonText: '',
    },
    discordTag: 'Discord Tag',
    requiredDiscordTag:
      'Ein Discord-Tag ist erforderlich, bitte geben Sie einen gültigen Discord-Tag an.',
    invalidDiscordTag: 'Ungültiger Discord-Tag',
    invalidDiscordTagFull:
      'Ungültiger Discord-Tag. Bitte geben Sie einen gültigen Discord-Tag an.',
    email: 'Email',
    requiredEmail:
      'Eine E-Mail ist erforderlich, bitte geben Sie eine gültige E-Mail Adresse an.',
    invalidEmail: 'Ungültige E-Mail',
    invalidEmailFull:
      'Ungültige E-Mail. Bitte geben Sie eine gültige E-Mail ein.',
    message: 'Nachricht',
    submit: 'Senden Sie',
    success: 'Gesendet',
    thankYou:
      'Vielen Dank, dass Sie sich mit uns in Verbindung gesetzt haben, wir werden Ihre Nachricht so schnell wie möglich beantworten!',
  },
  onlineText: 'Online',
};

export default language;
