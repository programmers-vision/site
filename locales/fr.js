const discordButton = 'REJOIGNEZ LE DISCORD';
const staffApplicationTitle = 'Candidature Du Personnel';
const contactUsTitle = 'Nous contacter';

const language = {
  languageName: 'Français',
  landing: {
    subHeading: 'Une communauté de programmation',
    buttonText: 'REJOINDRE LA COMMUNAUTÉ',
  },
  navigation: [
    'Accueil',
    staffApplicationTitle,
    'Faire Un Don',
    contactUsTitle,
  ],
  home: {
    section1: {
      heading: 'Qui Sommes Nous?',
      subHeading: "Une communauté de développeurs qui s'entraident",
      paragraph:
        "Programmers Palace est une communauté amicale et solidaire pour les programmeurs de tous niveaux d'expérience. Nous offrons de l'aide dans une variété de langages de programmation et de niveaux. En plus d'un support en direct constant, nous organisons des événements et proposons régulièrement des cours de programmation et de conception Web. Si vous êtes passionné par la programmation et que vous souhaitez rencontrer des personnes partageant les mêmes idées, le palais des programmeurs est l'endroit idéal.",
      buttonText: discordButton,
    },
    section2: {
      heading: 'Pourquoi Nous Rejoindre?',
      subHeading: 'Apprendre, enseigner ou simplement passer du temps',
      paragraph:
        "Que vous débutiez ou que vous ayez des années d'expérience, nous avons une place pour vous. Notre objectif est de rendre la programmation agréable tout en offrant un environnement social hospitalier pour que les gens se connectent et développent leurs compétences en coopération. Rejoignez-nous pour rencontrer de nouvelles personnes, développer vos compétences, recevoir de l'aide ou aider d'autres personnes à résoudre des problèmes de code, et assister à des conférences données par nos membres expérimentés. Notre communauté se développe rapidement, avec des milliers de développeurs du monde entier qui se réunissent pour approfondir leurs connaissances et passer un bon moment.",
      buttonText: '',
    },
    section3: {
      heading: 'Je Suis Intéressé! Comment Puis-Je M’inscrire?',
      subHeading: 'Notre communauté est hébergée sur Discord',
      paragraph:
        "Discord est notre plateforme de choix en raison de ses fonctionnalités, de sa facilité d'utilisation et de sa fiabilité. Si vous avez déjà un compte Discord et que vous souhaitez vous inscrire, cliquez sur le bouton ci-dessous pour obtenir le lien d'invitation. Si vous n'en avez pas, vous pouvez facilement en créer un gratuitement et l'utiliser facilement sur n'importe quelle plate-forme, y compris les navigateurs Web.",
      buttonText: discordButton,
    },
  },
  form: {
    formIncomplete:
      'Veuillez remplir les champs obligatoires indiqués par un astérisque (*).',
    submissionError:
      'Un problème est survenu. Veuillez réessayer plus tard ou contacter notre équipe.',
    failure:
      'Quelque chose ne va pas, veuillez revoir les erreurs et corriger votre candidature.',
  },
  staffApplication: {
    title: staffApplicationTitle,
    section: {
      heading: '',
      subHeading: 'Voulez-vous faire partie de notre équipe ?',
      paragraph:
        "Nous sommes constamment à la recherche de membres actifs pour rejoindre notre équipe et nous aider à construire notre communauté. Notre équipe se compose de plusieurs rôles avec des responsabilités différentes. Les membres de l'équipe travaillent ensemble pour modérer le serveur, aider les membres du serveur et contribuer à la croissance du serveur. Si vous souhaitez rejoindre notre équipe, veuillez remplir le formulaire ci-dessous et nous vous contacterons.",
      buttonText: '',
    },
    discordTag: 'Tag Discord',
    requiredDiscordTag:
      'Un tag discord est requis, veuillez fournir un tag discord valide.',
    invalidDiscordTag: 'Tag Discord non valide',
    invalidDiscordTagFull:
      'Tag Discord non valide. Veuillez entrer un Tag Discord valide.',
    email: 'Email',
    requiredEmail:
      'Un email est requis, veuillez fournir une adresse email valide.',
    invalidEmail: 'Email non valide',
    invalidEmailFull:
      'Courriel non valide. Veuillez entrer une adresse électronique valide.',
    positionType: 'Pour quel poste souhaitez-vous postuler ?',
    positionRequired: 'Choisissez le poste pour lequel vous postulez.',
    invalidPosition: 'Position non valide',
    selectPosition: 'Sélectionnez une position',
    position: {
      moderator: 'Modérateur',
      community: 'Engagement communautaire',
      teacher: 'Éducateur',
      developer: 'Développeur',
    },
    timeOnDiscord: 'Quel est votre temps moyen quotidien sur Discord ?',
    requiredTimeOnDiscord:
      "Indiquez le nombre d'heures passées quotidiennement sur Discord.",
    invalidTimeOnDiscord:
      "Heures non valides. Veuillez saisir un nombre d'heures valide.",
    joinReason: 'Pourquoi voulez-vous rejoindre notre équipe ?',
    experience:
      "Quelle est votre expérience en matière de programmation ? Pour les postes d'engagement social, décrivez également les expériences pertinentes.",
    whatsSpecial: 'Pourquoi devrions-nous vous considérer ?',
    serverGoals:
      'Quels sont vos objectifs personnels ou vos objectifs pour le serveur ?',
    additional: 'Vous souhaitez ajouter quelque chose ?',
    submissionNote:
      "* Veuillez contacter Canter si vous n'avez pas de réponse dans la semaine.",
    thankYou:
      'Merci de votre intérêt à rejoindre notre équipe, nous vous contacterons dès que possible !',
    submit: 'Soumettre la candidature',
    success: 'Soumis',
  },
  contactUs: {
    title: contactUsTitle,
    section: {
      heading: '',
      subHeading: 'Votre opinion compte pour nous!',
      paragraph:
        "Nous aspirons à être la meilleure communauté de programmation, et nous sommes toujours ouverts aux commentaires et aux partenariats ! Si vous souhaitez entrer en contact avec notre équipe ou simplement partager vos suggestions, plaintes et commentaires, veuillez utiliser le formulaire ci-dessous. Nous sommes impatients de recevoir vos commentaires concernant votre expérience et votre relation avec nous. Merci d'avance pour votre réponse.",
      buttonText: '',
    },
    discordTag: 'Tag Discord',
    requiredDiscordTag:
      'Un tag discord est requis, veuillez fournir un tag discord valide.',
    invalidDiscordTag: 'Tag Discord non valide',
    invalidDiscordTagFull:
      'Tag Discord non valide. Veuillez entrer un Tag Discord valide.',
    email: 'Email',
    requiredEmail:
      'Un email est requis, veuillez fournir une adresse email valide.',
    invalidEmail: 'Email non valide',
    invalidEmailFull:
      'Courriel non valide. Veuillez entrer une adresse électronique valide.',
    message: 'Message',
    submit: 'Envoyer',
    success: 'Envoyé',
    thankYou:
      'Merci de nous avoir contactés, nous répondrons à votre message dès que possible!',
  },
  onlineText: 'En Ligne',
};

export default language;
