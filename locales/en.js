const discordButton = 'JOIN THE DISCORD';
const staffApplicationTitle = 'Staff Application';
const contactUsTitle = 'Contact Us';

const language = {
  languageName: 'English',
  landing: {
    subHeading: 'A Programming Community',
    buttonText: 'JOIN THE COMMUNITY',
  },
  navigation: ['Home', staffApplicationTitle, 'Donate', contactUsTitle],
  home: {
    section1: {
      heading: 'Who Are We?',
      subHeading: 'A Community of Developers That Help Each Other',
      paragraph:
        "Programmers Palace is a friendly and supportive community for programmers of all levels of experience. We offer help in a variety of programming languages and levels. In addition to constant live support, We host events and provide programming and web design courses on a regular basis. If you're passionate about programming and would like to meet like-minded individuals, programmers palace is the place to be.",
      buttonText: discordButton,
    },
    section2: {
      heading: 'Why Join Us?',
      subHeading: 'Learn, Teach, or Simply Hang Out',
      paragraph:
        "Whether you're just starting out or have years of experience, we have a place for you. Our objective is to make programming enjoyable while also providing a hospitable social environment for people to connect and expand their skills cooperatively. Join us to meet new people, develop your skill set, receive help or assist others in resolving code issues, and attend lectures given by our experienced members. Our community is rapidly growing, with thousands of developers from all over the world coming together to expand their knowledge and have a great time. ",
      buttonText: '',
    },
    section3: {
      heading: "I'm Interested! How Do I Join?",
      subHeading: 'Our Community Is Hosted on Discord',
      paragraph:
        "Discord is our platform of choice due to its functionalities, ease of use, and reliability. If you already have a Discord account and would like to join, click the button below to get the invite link. If you don't have one, you can easily create one for free and conveniently use it on any platform, including web browsers. ",
      buttonText: discordButton,
    },
  },
  form: {
    formIncomplete:
      'Please complete the required fields indicated with an asterisk (*).',
    submissionError:
      'Something went wrong, please try again later or contact our staff team.',
    failure:
      'Something is not right, please review the errors and correct your submission.',
  },
  staffApplication: {
    title: staffApplicationTitle,
    section: {
      heading: '',
      subHeading: 'Do You Want to Be a Part of Our Team?',
      paragraph:
        "We are constantly looking for active members to join our team and help us in building our community. Our team consists of several roles with varying responsibilities. Staff members work together to moderate the server, help server members, and contribute to the server's growth. If you would like to join our team, please fill out the form below and we will get back to you.",
      buttonText: '',
    },
    discordTag: 'Discord Tag',
    requiredDiscordTag:
      'A discord tag is required, please provide a valid discord tag.',
    invalidDiscordTag: 'Invalid Discord Tag',
    invalidDiscordTagFull:
      'Invalid Discord Tag. Please enter a valid Discord Tag.',
    email: 'Email',
    requiredEmail:
      'An email is required, please provide a valid email address.',
    invalidEmail: 'Invalid Email',
    invalidEmailFull: 'Invalid Email. Please enter a valid Email.',
    positionType: 'What position would you like to apply for?',
    positionRequired: 'Choose the position for which you are applying.',
    invalidPosition: 'Invalid Position',
    selectPosition: 'Select a position',
    position: {
      moderator: 'Moderator',
      community: 'Community Outreach',
      teacher: 'Teacher',
      developer: 'Developer',
    },
    timeOnDiscord: "What's your daily average time on Discord?",
    requiredTimeOnDiscord:
      'Specify the amount of hours spent on Discord daily.',
    invalidTimeOnDiscord:
      'Invalid Hours. Please enter a valid amount of hours.',
    joinReason: 'Why do you want to join our staff team?',
    experience:
      'What is your experience with programming? For social engagement positions describe relevant experiences as well.',
    whatsSpecial: 'Why should we consider you?',
    serverGoals: 'What are your personal goals or goals for the server?',
    additional: 'Anything else you would like to add?',
    submissionNote:
      "* Please contact canter if you don't hear back within a week.",
    thankYou:
      'Thank you for your interest in joining our team, we will contact you as soon as possible!',
    submit: 'Submit Application',
    success: 'Submitted',
  },
  contactUs: {
    title: contactUsTitle,
    section: {
      heading: '',
      subHeading: 'Your Opinion Matters to Us!',
      paragraph:
        "We aspire to be the best programming community, and we're always open to feedback and partnerships! If you would like to get in touch with our staff team or simply share your suggestions, complaints, and comments, please use the form below. We look forward to receiving feedback regarding your experience and relationship with us. Thank you in advance for your response.",
      buttonText: '',
    },
    discordTag: 'Discord Tag',
    requiredDiscordTag:
      'A discord tag is required, please provide a valid discord tag.',
    invalidDiscordTag: 'Invalid Discord Tag',
    invalidDiscordTagFull:
      'Invalid Discord Tag. Please enter a valid Discord Tag.',
    email: 'Email',
    requiredEmail:
      'An email is required, please provide a valid email address.',
    invalidEmail: 'Invalid Email',
    invalidEmailFull: 'Invalid Email. Please enter a valid Email.',
    message: 'Message',
    submit: 'Send',
    success: 'Sent',
    thankYou:
      'Thank you for contacting us, we will reply to your message as soon as possible!',
  },
  onlineText: 'Online',
};

export default language;
