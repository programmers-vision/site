import en from '../locales/en'
import fr from '../locales/fr'
import ru from '../locales/ru'
import de from '../locales/de'

let flagLink = (flagCode) => {
  return flagsUrl + flagCode + flagImgType
}
let flagsUrl = 'https://hatscripts.github.io/circle-flags/flags/'
let flagImgType = '.svg'

export const locales = {
  en: {
    definition: {
      code: 'en',
      name: 'English',
      flag: flagLink('gb'),
    },
    messages: en,
  },
  fr: {
    definition: {
      code: 'fr',
      name: 'Français',
      flag: flagLink('fr'),
    },
    messages: fr,
  },
  ru: {
    definition: {
      code: 'ru',
      name: 'Русский',
      flag: flagLink('ru'),
    },
    messages: ru,
  },
  de: {
    definition: {
      code: 'de',
      name: 'Deutsch',
      flag: flagLink('de'),
    },
    messages: de,
  },
}

export const config = {
  locale: 'en',
  fallbackLocale: 'en',
  messages: Object.keys(locales).reduce((map, conf) => {
    map[conf] = locales[conf].messages
    return map
  }, {}),
}
