# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.6] - 2022-05-20
### Changed
- Tweaked Nuxt Router loading bar
- Minor Components Refactor

## [1.2.5] - 2022-05-20
### Added
- Default Layout which contains the header and footer
- Nuxt Router Page transitions
- Webp format to logo in navbar

### Changed
- Tweaked height of page header
- The Twitter meta card is now small by default, with the large card image appearing exclusively for the home page

## [1.2.4] - 2022-05-19
### Added
- Image optimization using the Nuxt image component
- Implemented image prioritization, image lazy loading, and image resizing 
- Customized Submit button text for each form
- Twitter Meta Tags

### Changed
- Major Refactor for the section component
- File restructure for components 
- Moved images from assets to static
- Other tweaks


## [1.2.3] - 2022-05-18
### Added
- Added Confetti effect on form submission 
- Added form-submit, form text-area, and page-header components

### Changed
- Changed the post submission section in the staff and contact forms 
- Broke down the staff and contact pages for simplicity
- Renamed "css" file to "styles"
- CSS file was divided into multiple files so that only the necessary styles from each page are imported
- Created sub folders inside components to separate the components
- Other tweaks

## [1.2.2] - 2022-05-17
### Added
- Added an open graph description and URL
- Added width and height attributes to the sections images to reduce cumulative layout shift

## [1.2.1] - 2022-05-15
### Added
- Added an Alt tag for the section images to improve accessibility & SEO
- Added image width and height to reduce cumulative layout shift
- Added Open Graph Meta Tags for SEO optimization and social media preview

### Changed
- Changed the navbar functionality in the navbar data file to include a "external" option so that the user would be provided the proper link depending on whether it was within the website or external.

## [1.2.0] - 2022-05-12
### Added
-  Added a Contact Us Page

### Changed
- Changed formatting style to include semicolons
- Refactored locales files and sections-data
- Other small tweaks

## [1.1.1] - 2022-05-11
### Added
- Added a submission note to contact Canter if the user doesn't hear back within a week.
- Added an error toast if Axios request fails

### Removed
- Removed accept terms on submission

## [1.1.0] - 2022-05-05
### Added
- added in-built staff-application instead of the Google Form redirect
- added relevant localization for the staff-application

### Changed
- resolved lang-switcher bug, which now doesn't cause a language reset on page navigation

### Removed
- Husky
- Commitlint

## [1.0.0] - 2022-04-10
### Added
- landing page with an introduction and links to the Discord Community
- link to the Google Form for the Staff Application
- link for donations via PayPal
- localization/i18n with 4 available languages (en, de, fr, ru)
- visible online status of the Discord Community
- social media links in the footer
